<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLessonsadvsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lessonsadvs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('bruid')->nullable();
            $table->string('title');
            $table->text('summary')->nullable();
            $table->text('tasks')->nullable();
            $table->text('columns')->nullable();
            $table->text('columnOrder')->nullable();
            $table->string('extref1', 256)->nullable();
            $table->string('extref2', 256)->nullable();
            $table->string('extref3', 256)->nullable();
            $table->string('extref4', 256)->nullable();
            $table->string('extref5', 256)->nullable();
            $table->text('exfield0')->nullable();
            $table->text('exfield1')->nullable();
            $table->text('exfield2')->nullable();
            $table->text('exfield3')->nullable();
            $table->text('exfield4')->nullable();
            $table->string('author', 128)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('lessonsadvs');
    }
}
