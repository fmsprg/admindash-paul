<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePostAdvertsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('post__adverts', function (Blueprint $table) {
            $table->increments('id');
            $table->string('job_title');
            $table->string('client');
            $table->string('location');
            $table->string('countryCode');
            $table->string('category');
            $table->string('industry')->nullable();
            $table->string('number_of_positions')->nullable();
            $table->string('contract_duration');
            $table->string('contract_type')->nullable();
            $table->string('minimum_salary')->nullable();
            $table->string('maximum_salary')->nullable();
            $table->string('currency')->nullable();
            $table->string('payment_cycle')->nullable();
            $table->string('job_description');
            $table->string('extref1', 256)->nullable();
            $table->string('extref2', 256)->nullable();
            $table->string('extref3', 256)->nullable();
            $table->text('exfield0')->nullable();
            $table->text('exfield1')->nullable();
            $table->text('exfield2')->nullable();
            $table->text('exfield3')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('post__adverts');
    }
}
