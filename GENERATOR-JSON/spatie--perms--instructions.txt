0. Generator.

php artisan infyom:scaffold $MODEL_NAME --fieldsFile=filename_from_model_schema_directory

1. Like this to make a new user.

$user = \App\Models\User::factory()->create(['name' => 'Tempuser1','email' =>'tempuser1@example.com',]);

2. With the $user object still active:

$user->assignRole('writer');

3. And check

$user->hasRole('writer');



4. If there is no user object yet, DO NOT use 'where', use a return-1-record-only operator:

$user=\App\Models\User::firstOrNew(['id'=>4]);


5. assign:

$user->assignRole('writer');  //if WHERE was used, the $user object will not permit the assignRole method.

6. Try a non-esiting role (it will not work)

$user->assignRole('baker');

 
